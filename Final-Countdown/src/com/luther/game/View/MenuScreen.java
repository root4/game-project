package com.luther.game.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.luther.game.Assets;
import com.luther.game.Root;
import com.luther.game.Model.Player;

/**
 * <h1>Purpose: </h1> 
 *  	Main screen, animates spining world and gives game menu
 * 
 * @author Luther Root
 * 
 * 
 */
public class MenuScreen implements Screen {

	public boolean init = false;

	Stage stage;
	SpriteBatch batch;
	Root game;
	Table table;
	TextButtonStyle textButtonStyle;
	LabelStyle labelStyle;
	Pixmap pixmap;

	TextButton button;
	 TextButton button2; 
	 TextButton button3;
	
	 
	 Color color = new Color(.5f, 1f, 1f, 1f );
	 
	//
	// main screen items
	
	public float ROTATION = .5f;
	Texture tex;
	Texture play;
	
	Image image;
	Image playImage;
	ImageButton playButton;
	float originx;
	float originy;
	public boolean stop = false;
	TextureRegion region;
	
	

	TextureRegion bframe;
	float tTime=0;
	
	 Sprite sdarkClouds[] = new Sprite[5];
	Sprite darkClouds[] = new Sprite[2];
	
	//
	
	TextureRegion frame;
	
	float FRAMERATEDURATION = 0.2f;
	float statetime = 0;
	float width1= 0;
	Player p1;
	public boolean step = false;
	boolean animate = true;
	
float	width = Root.gameWidth;
	float height = Root.gameHeight;
	
	public MenuScreen( Root g1, boolean showAnim) {
		
		
		
		this.game = g1;
		step = !showAnim;
		tex = new Texture(Gdx.files.internal("data/opening/one.png"));
		region = new TextureRegion(tex,0,0,121,122);
		play = new Texture(Gdx.files.internal("playButton.png"));
		p1 = new Player(width/2 -12, height - 64,64,64);
		
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);

		image = new Image(region);
		image.setWidth(width);
		image.setHeight(width);
		image.setPosition(0,-height);
		playImage = new Image(play);
		
		playImage.setWidth(width /9);
		playImage.setHeight(width /9);
		playImage.setPosition(width/2 - playImage.getWidth()/2, height/2- playImage.getHeight()/2);
		p1.getPosition().set(width/2 -12, height/1.65f);
		Gdx.input.setInputProcessor(stage);
		playImage.addListener(new ClickListener(){
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				stop = true;
				return true;
				
			}
		});
		Camera cam = new OrthographicCamera(width,height);
		cam.position.set(width/2, height/2, 0);
		
		stage.setCamera(cam );
		
		batch = stage.getSpriteBatch();
		stage.addActor(image);
		stage.addActor(playImage);
		
		
		
		  for(int i = 0; i<5; i++)
		  {
			  if (i<2)
			  {
				  sdarkClouds[i] = new Sprite(Assets.sdcloud);
				   sdarkClouds[i].setBounds(0 -  200, height -44 ,200,50);
			  }
			  else
			  {
				  sdarkClouds[i] = new Sprite(Assets.sdcloud);
			  	sdarkClouds[i].setBounds(width +  sdarkClouds[i].getWidth(), height-44,200,50);
			  }
				  
		  }
		 darkClouds[0] = new Sprite(Assets.dcloud);
		 darkClouds[0].setBounds(- width/4, height/1.6f,200,100);
		 darkClouds[1] = new Sprite(Assets.dcloud);
		 darkClouds[1].setBounds(width + width/5.5f, height - height/3,200,100);
		
			}
		
		

	

	@Override
	public void resize(int width, int height) {
	}			
		

	@Override
	public void dispose() {
		
	}

	@Override
	public void render(float delta) {

		  Gdx.gl.glClearColor( color.r,color.g,color.b,color.a);
	        Gdx.gl.glClear( GL10.GL_COLOR_BUFFER_BIT );
	        float dt = Gdx.graphics.getRawDeltaTime();
	        stage.act(Math.min(Gdx.graphics.getRawDeltaTime(), 1/30f));
	    draw(batch,dt);
	
		//Table.drawDebug(stage);
	}

	private void animateTable() {
		float original = table.getY();
		
		table.setPosition(0, original/1.05f);
		
	}
	public void drawAnimation(SpriteBatch batch, float dt)
	{
		fadeout();
		if(init == false)
			init();
		init = true;
		statetime+=dt;
		animateTable();
		
		
		
		p1.update(dt);
		batch.begin();	
		
		if(statetime<4&& animate == true)
		{
		frame = Assets.world.getKeyFrame(statetime, false);
		
		move();
		}
		else 
		{animate = false;
			statetime = 0;
			
			}
		batch.draw(frame,0,-width/1.6f,width,width);
		if(p1.getPosition().y < 0)
		{
			p1.getVelocity().set(0, 0);
			p1.getPosition().y = 0;
		}
		else if((table.getY() - table.getHeight() )< p1.getPosition().y)
			p1.getVelocity().y -= 20;

		p1.draw(batch);
		for (int i = 0; i<5; i++)
			sdarkClouds[i].draw(batch);
		for(int i = 0;i<2;i++)
			darkClouds[i].draw(batch);
		
		batch.end();
	
		stage.draw();
		
		
		
	}
	private void init() {
		
		stage.clear();
		Camera cam = new OrthographicCamera(width,height);
		cam.position.set(width/2, height/2, 0);
		
		stage.setCamera(cam );
		
		batch = stage.getSpriteBatch();
		
			 table = new Table(Root.skin.skin);
	
			
			
				//table.setFillParent(true);
				table.setWidth(width);
				table.setHeight(height);
				stage.addActor(table);
				table.debug();

			

				final Label title = new Label("Luke's Game", Root.skin.skin.get("menu", LabelStyle.class));
				
				
			
				
			 button = new TextButton("Play", Root.skin.skin.get("menu", TextButtonStyle.class));
			
			 button2 = new TextButton("Help", Root.skin.skin.get("menu", TextButtonStyle.class));
			 button3 = new TextButton("EXIT", Root.skin.skin.get("menu", TextButtonStyle.class));
			
				button.pad(15, 50, 15, 50);
			
				button2.pad(15, 50, 15, 50);
				button3.pad(15, 50, 15, 50);

				table.add(title);
				table.row();
				table.add(button).width(150).space(15);
				
				table.row();

				table.add(button2).width(150).space(15);
				table.row();
				table.add(button3).width(150).space(15);
				table.row();

				table.setPosition(0, height);
				table.setBackground(Root.skin.skin.newDrawable("white", 0,0,0,0));
				
				
				Gdx.input.setInputProcessor(stage);
				
				button.addListener(new ClickListener()
				{
					@Override
					public void clicked(InputEvent event,
					           float x,
					           float y)
					{
						//System.out.println("Clicked! Is checked: " + button.isChecked());
						//Gdx.input.setInputProcessor(game.gameScreen.input);
						//game.gameScreen.loadNew(new Vector2(Root.prefs.getFarthestWorld(),
						//		Root.prefs.getFarthestLevel()));
						//game.setScreen(game.gameScreen);
						game.setScreen(Root.lScreen);
						
					}
				});
				button2.addListener(new ClickListener()
				{
					@Override
					public void clicked(InputEvent event,
					           float x,
					           float y)
					{
						//Gdx.input.setInputProcessor(lSelect.stage);
					
						game.setScreen(Root.helpScreen);
					}
				});
				button3.addListener(new ClickListener()
				{
					@Override
					public void clicked(InputEvent event,
					           float x,
					           float y)
					{
						Gdx.app.exit();

					}
				});
		
	}
	public void drawRotating(SpriteBatch batch, float dt){
		  // update and draw the stage actors
		
        originx= image.getWidth()/2;
    	originy = image.getHeight()/2;
		if(!stop)
    	{
    		if(image.getRotation() == 360)
    			image.setRotation(0);
        image.rotate(ROTATION);
       
    	}
    	else
    		rotateTo();
		
		
    	p1.update(dt);
    	tTime+=dt;
    	bframe = Assets.bird.getKeyFrame(tTime);
    	
        image.setOrigin(originx, originy);
       
		stage.draw();
		batch.begin();
		batch.draw(bframe, 100, 480/1.5f,50,50);
		batch.draw(Assets.sun, 0, height - width/6   , width/6,width/6);
		batch.draw(Assets.wcloud, width - width/4,height - width /6.5f,width /6,width/8f);
		batch.draw(Assets.wcloud, width /4, height - width/8,width /6,width/9);
		 //  p1.menuDraw(batch, image.getRotation(), new Vector2(25,-image.getHeight()/1.5f ));
		p1.runDraw(batch);
		batch.end();
		
	}
	
	public void draw(SpriteBatch batch, float dt){
	
		
		
		
		if(step == false)
			drawRotating(batch,dt);
		else
			drawAnimation(batch,dt);
	}
	public void rotateTo()
	{
		float original = image.getRotation();
		if(original >1)
		image.rotate(-original /4);
		else 
			step = true;
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);		

	}

	@Override
	public void hide() {

		this.dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	public boolean keyUp(int keycode) {
		if (keycode == Keys.BACK)
			Gdx.app.exit();
		return false;
	}

	public void move(){
		float da = (width/2 );
		float db = (width/3 );
		float dc = (width/15) ;
		
		float x = sdarkClouds[0].getBoundingRectangle().x;
				
			 x +=  ( da - sdarkClouds[0].getBoundingRectangle().x ) * .1f;
		
		
		 sdarkClouds[0].setPosition(x -10, sdarkClouds[0].getBoundingRectangle().y);
		
				
		 
		float y  = sdarkClouds[1].getX();
		y += (dc - sdarkClouds[1].getX()) * .1f;
		 sdarkClouds[1].setPosition(y-10, sdarkClouds[1].getY());
		 
			float z  = sdarkClouds[2].getX();
			z += (db - sdarkClouds[2].getX()) * .1f;
			 sdarkClouds[2].setPosition(z-10, sdarkClouds[2].getY());
			 
			 float a  = sdarkClouds[3].getX();
				a += (-db + width - sdarkClouds[3].getX()) * .1f;
				 sdarkClouds[3].setPosition(a-10, sdarkClouds[3].getY());
		 
				 float b  = sdarkClouds[4].getX();
					b += (- dc + width - sdarkClouds[4].getX()) * .1f;
					 sdarkClouds[4].setPosition(b-10, sdarkClouds[4].getY());
					 
					 float c = darkClouds[0].getX();
					
						c += (db - darkClouds[0].getX()) * .1f;
						 darkClouds[0].setPosition(c-15, darkClouds[0].getY());
						 
						 float d = darkClouds[1].getX();
							
							d += (-db + width - darkClouds[1].getX()) * .1f;
							 darkClouds[1].setPosition(d-5, darkClouds[1].getY());
		
	}
	void fadeout()
	{
	color.lerp(.3f,.3f, .3f, 1f, .1f);
		
	}
}
