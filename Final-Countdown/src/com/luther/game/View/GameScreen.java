package com.luther.game.View;

import java.text.DecimalFormat;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.luther.game.Assets;
import com.luther.game.ProjectSkin;
import com.luther.game.Root;
import com.luther.game.Utilities;
import com.luther.game.Controller.Level;
import com.luther.game.Controller.InputHandler;
import com.luther.game.Model.Button;
import com.luther.game.Model.Player;
import com.luther.game.Model.Player.Status;
import com.luther.game.View.ReadyScreen.ReadyType;
/**
 * <h1>Purpose: </h1> 
 *  	Our main game screen.
 *  	
 * 
 * @author Luther Root
 * 
 * 
 */
public class GameScreen implements Screen {

	enum gameState {
		READY, RUNNING, PAUSED, LEVELCOMPLETED, GAMEOVER
	};

	float accum = 0;

	gameState state;
	Root game;

	LevelRenderer levelRenderer;
	float averageDt = 0;
	float cycles = 0;

	public Button pause;
	public PauseMenu pauseMenu;
	
	private SpriteBatch batch;
	private Player p1;
	private FPSLogger fpslogger;
	private OrthographicCamera cam;
	private ShapeRenderer shapes;

	public static Vector2 pointer;
	
	float lastDt = 1 / 60f;
	float dt = 1 / 60f;
	InputHandler input;
	Level level;

	float gdxHeight, gdxWidth;

	String nextLevel;

	public Vector2 cameraObjective = new Vector2(0, 0);

	FrameBuffer fbo;
	Texture levelBuffer;
	TextureRegion lreg;
	public ReadyScreen readyScreen;
	public LevelCompleteScreen lcScreen;
	public float elapsedTime;
	DecimalFormat format = new DecimalFormat("0.00");
	

	public GameScreen(Root game1) {

		this.game = game1;
		gdxWidth = 800;
		gdxHeight = 480;

	

	}
	public void init(){
		pauseMenu = new PauseMenu(game);
		
		p1 = new Player(90, 120, 32, 32);

		cam = new OrthographicCamera();
		readyScreen = new ReadyScreen(this);
		lcScreen = new LevelCompleteScreen(this);
		gdxWidth = 800;
		gdxHeight = 480;
		pause = new Button(Gdx.graphics.getWidth() - 64, Gdx.graphics.getHeight() -64,
				64,
				64);
		
	
		pointer = new Vector2(-1, -1);

		batch = new SpriteBatch();
		
		shapes = new ShapeRenderer();

		fpslogger = new FPSLogger();

		level = new Level();
		
		levelRenderer = new LevelRenderer(Root.prefs.getWorld(),
				level.getColTiles(), level.getEnemies(), level.mapSizeX,
				level.mapSizeY);

		input = new InputHandler(p1);
		
	}

	public void loadNew(Vector2 worldLevel) {
	//Root.prefs.setFarthestLevel(5);Root.prefs.setFarthestWorld(5);Root.prefs.setWorld(0);Root.prefs.setLevel(0);
				
		if(worldLevel.x >=5)
		{
			worldLevel = new Vector2(0,0);
			game.setScreen(Root.menuScreen);
		}
		Root.prefs.setWorld((int) worldLevel.x);
		Root.prefs.setLevel((int) worldLevel.y);

		String loadLevel = new String("");

		if (worldLevel.x == 0)
			loadLevel = "data/world/level1/";
		if (worldLevel.x == 1)
			loadLevel = "data/world/level2/";
		if (worldLevel.x == 2)
			loadLevel = "data/world/level3/";
		if (worldLevel.x == 3)
			loadLevel = "data/world/level4/";
		if (worldLevel.x == 4)
			loadLevel = "data/world/level5/";
		System.out.println(loadLevel);
		Assets.loadLevel(loadLevel);

		

		cam.setToOrtho(false,800,480);
		
		loadLevel = loadLevel + "map" + (int) worldLevel.y + ".TXT";

	//	System.out.println(MyGdxGame.prefs.getWorld() + " "
			//	+ MyGdxGame.prefs.getLevel());

		level.loadMap(loadLevel);
	
		
	//	Gdx.input.setInputProcessor(input);
		
		fbo = new FrameBuffer(Pixmap.Format.RGB888, level.mapSizeX * 32,
				level.mapSizeY * 34, false);

		levelRenderer.MapSizeX = level.mapSizeX;
		levelRenderer.MapSizeY = level.mapSizeY;
		levelBuffer = levelRenderer.render(batch, fbo);
		lreg = new TextureRegion(levelBuffer);
		lreg.flip(false, true);
		p1.Reset();
		readyScreen.type = ReadyType.READY;
			state = gameState.READY;
		this.update(1 / 60f);

		System.out.println(loadLevel);
		
	

	}

	

	public void update(float delta) {
		switch (state) {

		case READY:
			updateReady();
			break;

		case RUNNING:
			updateRunning(delta);
			break;

		case PAUSED:
			updatePaused();
			break;

		case LEVELCOMPLETED:
			updateLevelComplete(); 
			break;

		case GAMEOVER:
			updateGameOver();
			break;

		}
	}

	private void updateReady() {
		
		readyScreen.update();
		elapsedTime = 0;

	}

	private void updateLevelComplete() {
		
		
		Root.prefs.setTime(new Vector2(Root.prefs.getWorld(), Root.prefs.getLevel()),elapsedTime);
		

	}

	private void updatePaused() {

		//p1.setVelocity(0, 0);

		if (Gdx.input.justTouched()) {
			// cam.unproject(Gdx.input.getX(),Gdx.input.getY(),0);
			pointer.x = Gdx.input.getX();

			pointer.y = -Gdx.input.getY() +gdxHeight;
			
			


			
		}

	}

	private void updateGameOver() {

		// game.setScreen(new GameScreen(game,new
		// Vector2(Float.valueOf(prefs.getWorld()),Float.valueOf(prefs.getLevel()))));
		// p1 = new Player(90,90,32,32);

		
		p1.Reset();
		state = gameState.READY;
	}

	public void updateRunning(float delta) {

		elapsedTime+=delta;
		if (p1.getState() == Status.DEAD) {
			state = gameState.GAMEOVER;
		}
		if (p1.getState() == Status.WIN) {
			state = gameState.LEVELCOMPLETED;
		}

		if (Gdx.input.justTouched()) {
			pointer.x = Gdx.input.getX();
			pointer.y = -Gdx.input.getY() + gdxHeight;

			System.out.println(pointer + "  " + pause.getBounds());
			
			if (Utilities.pointerInside(pointer, pause.getBounds())) {
				state = gameState.PAUSED;
			}

		}
		if (Gdx.input.isKeyPressed(Keys.W))
		{
			p1.setState(Status.WIN);
			
		}
		pointer.set(-1, -1);

		this.handleInput();

		level.CD(p1, delta);
		level.CDE(p1);
		
		p1.update(delta); // move
		levelRenderer.updateEnemies();

	}

	public void draw(float delta) {

		cameraObjective.set(p1.getPosition().x, p1.getPosition().y);

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		batch.setProjectionMatrix(cam.combined);

		switch (state) {

		case READY:
			Color c = batch.getColor();
			batch.setColor(c.r,c.g,c.b,.2f);
			drawRunning();
			batch.setColor(c);
			drawReady();
			break;
		case RUNNING:
			drawRunning();

			break;

		case LEVELCOMPLETED:
			drawLevelComplete();
			break;

		case GAMEOVER:
			drawGameOver();
			break;
		case PAUSED:
			drawRunning();
			drawPaused();
			break;

		default:
			update(delta);
			break;
		
		}
	
			
		

	

		this.updateCamera();

	}

	private void drawReady() {
		readyScreen.draw(batch);
		
	}
	private void drawPaused() {
		Matrix4 hudUi = cam.combined.cpy();
		hudUi.setToOrtho2D(0,0,gdxWidth, gdxHeight);
	
		batch.setProjectionMatrix(hudUi);
		batch.begin();
		pauseMenu.draw();
		batch.end();
	}

	private void drawGameOver() {
		p1.setVelocity(0, 0);
		// batch.draw(Assets.gameOver, Gdx.graphics.getWidth()/2,
		// Gdx.graphics.getHeight()/2);
		// cameraObjective.set(Gdx.graphics.getWidth()/2,
		// Gdx.graphics.getHeight()/2);

	}

	private void drawLevelComplete() {

		lcScreen.draw(batch);
	}

	public void drawRunning() {

		batch.setProjectionMatrix(cam.combined);
		batch.begin();
		batch.draw(lreg, 0, 0);
		levelRenderer.drawEnemies(batch);

		p1.draw(batch);
		
		drawHUD(batch);
		batch.end();
	}

	@Override
	public void render(float delta) {

		
		lastDt = dt;
		dt = Gdx.graphics.getRawDeltaTime();

		float interpolation = Interpolation.linear.apply(lastDt, dt, .9f);

		averageDt += interpolation;
		cycles++;

		update(1/60f);// check collisions get input
		

		draw(1/60f); // draw
		fpslogger.log();

		
		// System.out.println(averageDt / cycles);

	}

	@Override
	public void resize(int width, int height) {
		gdxWidth = width;
		gdxHeight = height;

	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

		if (state == gameState.RUNNING)
			state = gameState.PAUSED;
	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		p1.dispose();
		batch.dispose();
	}

	public void handleInput() {

		switch (Gdx.app.getType()) {
		case Android:
			input.getAndroidInput();
			break;
		case Desktop:
			input.getInput();
			break;
		case Applet:
			break;
		case WebGL:
			break;
		case iOS:
			break;
		default:
			break;

		}
	}

	public void drawHUD(SpriteBatch batch) {
		Matrix4 hudUi = cam.combined.cpy();
		hudUi.setToOrtho2D(0,0,gdxWidth,gdxHeight);                
		batch.setProjectionMatrix(hudUi);
		batch.draw(Assets.pauseButton, pause.getBounds().x, pause.getBounds().y, pause.getBounds().width, pause.getBounds().height);
		
	//	Assets.worldFont.draw(batch, Root.prefs.getWorld() + 1 + ":"
				//+ (Root.prefs.getLevel() + 1), 25,
			//	gdxHeight / 1.05f);
		Assets.font.draw(batch, ((Root.prefs.getWorld() + 1) + ":"+ (Root.prefs.getLevel() + 1) +" t: "+ format.format((elapsedTime)) + " s"), 25, gdxHeight/1.05f);
		if (Gdx.app.getType() == ApplicationType.Android) {

			batch.draw(Assets.controlRight, gdxWidth/3.6f, 0, gdxWidth/3.6f, gdxHeight/4.1f);
			batch.draw(Assets.controlUp, gdxWidth/1.7f, 0, gdxWidth/1.4f, gdxHeight/4.1f);
			batch.draw(Assets.controlLeft, 0, 0, gdxWidth/3.6f, gdxHeight/4.1f);
		}
		batch.end();
		float x = Player.JUMP - 600;
		float bar = gdxHeight/2;
		if (x >= 400)
			x = 400;
		 
		x /=400;
	
		shapes.begin(ShapeType.Filled);
		shapes.setColor(new Color(x, .2f/x , 0f, 1f)); // fade from green to blue
		shapes.rect(25, gdxHeight/ 3, 50, x*bar);
		shapes.end();
	
		shapes.begin(ShapeType.Line);
		shapes.setColor(Color.WHITE);
		shapes.rect(25, gdxHeight / 3, 50, bar);

		shapes.end();

		batch.begin();

	}

	public void updateCamera() {

		float lag = .1f;
		Vector3 position = cam.position;

		position.x += (cameraObjective.x - position.x) * lag;
		position.y += (cameraObjective.y - position.y) * lag;
		cam.position.set(position);
		cam.update();
	}

}
