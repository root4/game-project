package com.luther.game.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.luther.game.Root;
/**
 * <h1>Purpose: </h1> 
 *  	 A splash screen that displays my fake company
 * 
 * @author Luther Root
 * 
 * 
 */
public class SplashScreen implements Screen {

	Texture tex;
	TextureRegion rootGames;
	Stage stage;

	Root game;
	Image splashImage;

	public SplashScreen(Root game) {
		this.game = game;
		stage = new Stage();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
		if (Gdx.input.isTouched()) {

		} else if (splashImage.getActions().size == 0)
			game.setScreen(Root.menuScreen);

	}

	@Override
	public void resize(int width, int height) {

		stage.clear();

		// here we create the splash image actor and set its size
		splashImage = new Image(rootGames);
		splashImage.setWidth(width);
		splashImage.setHeight(height);

		// this is needed for the fade-in effect to work correctly; we're just
		// making the image completely transparent
		splashImage.getColor().a = 0f;

		// configure the fade-in/out effect on the splash image
		// Actions actions = Actions.$( FadeIn.$( 0.75f ), Delay.$( FadeOut.$(
		// 0.75f ), 1.75f ) );

		splashImage.addAction(Actions.sequence(Actions.fadeIn(1f),
				Actions.delay(3f), Actions.fadeOut(1f)));
		// and finally we add the actors to the stage, on the correct order
		stage.addActor(splashImage);

	}

	@Override
	public void show() {

		tex = new Texture(
				Gdx.files.internal("data/MenuStuffs/final root games.png"));
		tex.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		rootGames = new TextureRegion(tex, 0, 0, 512, 256);

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
