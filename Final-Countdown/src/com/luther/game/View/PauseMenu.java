package com.luther.game.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.luther.game.Root;
import com.luther.game.View.GameScreen.gameState;
import com.luther.game.View.ReadyScreen.ReadyType;
/**
 * <h1>Purpose: </h1> 
 *  	A pause menu, obviously
 * 
 * @author Luther Root
 * 
 * 
 */
public class PauseMenu {

	Root game;
	Stage stage;
	TextButton resume,menu,restart;
	Window window;
	Table table;
	
	public PauseMenu(final Root game)
	{
		this.game = game;
		stage = new Stage();
		resume = new TextButton("Resume", Root.skin.skin.get("pause",TextButtonStyle.class));
		menu= new TextButton("Menu", Root.skin.skin.get("pause",TextButtonStyle.class));
		restart = new TextButton("Restart", Root.skin.skin.get("pause",TextButtonStyle.class));
		window = new Window("Pause", Root.skin.skin.get("pause",WindowStyle.class));
		table = new Table();
		
		resume.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event,
			           float x,
			           float y)
			{
				Gdx.input.setInputProcessor(Root.gameScreen.input);
				Root.gameScreen.state = gameState.RUNNING;
			}
		});
		menu.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event,
			           float x,
			           float y)
			{
				Gdx.input.setInputProcessor(Root.menuScreen.stage);
				game.setScreen(Root.menuScreen);
			}
		});
		restart.addListener(new ClickListener()
		{
			@Override
			public void clicked(InputEvent event,
			           float x,
			           float y)
			{
				Root.gameScreen.state = gameState.GAMEOVER;
				Root.gameScreen.readyScreen.type = ReadyType.READY;
			}
		});
		//resume.pad(15, 50, 15, 50);
	//	menu.pad(15, 50, 15, 50);
		//restart.pad(15, 50, 15, 50);
		window.pad(15);
		window.padTop(45);
		window.add(resume).width(Gdx.graphics.getWidth()/4.5f).pad(15).row();
		window.add(menu).width(Gdx.graphics.getWidth()/4.5f).pad(15).row();
		window.add(restart).width(Gdx.graphics.getWidth()/4.5f).pad(15).row();
		table.add(window);
		table.setFillParent(true);
		stage.addActor(table);
		

	}
	
	public void draw()
	{
		Gdx.input.setInputProcessor(stage);
		stage.act();
		stage.draw();
		
	}
	
}