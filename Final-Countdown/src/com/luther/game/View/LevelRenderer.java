
package com.luther.game.View;

import java.util.List;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Matrix4;
import com.luther.game.Assets;
import com.luther.game.Model.CollisionTile;
import com.luther.game.Model.Enemy;
/**
 * <h1>Purpose: </h1> 
 *  	Render level and enemies
 * 
 * @author Luther Root
 * 
 * 
 */
public class LevelRenderer {

	List<CollisionTile> drawTiles;
	List<Enemy> enemies;

	public int MapSizeX;
	public int MapSizeY;

	final float BLOCKSIZE = 32;

	public LevelRenderer(int world, List<CollisionTile> Coltiles,
			List<Enemy> e, int x, int y) {

		enemies = e;
		MapSizeX = x;
		MapSizeY = y;
		drawTiles = Coltiles;

	}

	
	public Texture render(SpriteBatch batch, FrameBuffer fbo) {

		//Frame buffer is already set to width and height of map
		// Changes projection matrix to frame buffer
		Matrix4 matrix = new Matrix4();
		matrix.setToOrtho2D(0, 0, fbo.getWidth(), fbo.getHeight()); // change this to change size of openGL framebuffer

		batch.setProjectionMatrix(matrix);
		fbo.begin();
		batch.begin();

		batch.draw(Assets.back, 0, 0 + BLOCKSIZE, MapSizeX * BLOCKSIZE,
				MapSizeY * BLOCKSIZE);  // background

		for (CollisionTile tile : drawTiles) {  // Brute forced, but only runs once so doesn't effect performance 

			switch (tile.getTileType()) {

			case '1':
				batch.draw(Assets.one, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case '2':
				batch.draw(Assets.two, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case '3':
				batch.draw(Assets.three, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case '4':
				batch.draw(Assets.four, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case '5':
				batch.draw(Assets.five, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case '6':
				batch.draw(Assets.six, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case '7':
				batch.draw(Assets.seven, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case '8':
				batch.draw(Assets.eight, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case '9':
				batch.draw(Assets.nine, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case 'A':
				batch.draw(Assets.A, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case 'B':
				batch.draw(Assets.B, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case 'C':
				batch.draw(Assets.C, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case 'D':
				batch.draw(Assets.D, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case 'E':
				batch.draw(Assets.E, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case 'F':
				batch.draw(Assets.F, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case 'W':
				batch.draw(Assets.door, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height * 2);
				break;
			case 'Z':
				batch.draw(Assets.Z, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case 'U':
				batch.draw(Assets.U, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;
			case 'S':
				batch.draw(Assets.S, tile.getPosition().x,
						tile.getPosition().y, tile.getBounds().width,
						tile.getBounds().height);
				break;

			default:
				break;
			}
		}

		batch.end();
		fbo.end();

		return fbo.getColorBufferTexture();  // returns openGl texture residing in framebuffer object
	}

	public void drawEnemies(SpriteBatch batch) {
		float len = enemies.size();
		for (int i = 0; i < len; i++) {
			enemies.get(i).drawAndUpdateEnemy(batch);

		}
	}

	public void updateEnemies() {
		for (Enemy e : enemies) {
			e.update();

		}
	}

}
