package com.luther.game.View;

import java.text.DecimalFormat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.luther.game.Assets;
import com.luther.game.ProjectSkin;
import com.luther.game.Root;
import com.luther.game.View.GameScreen.gameState;
import com.luther.game.View.ReadyScreen.ReadyType;

/**
 * <h1>Purpose: </h1> 
 *  	Displays a message that the level is complete along with best time.
 * 
 * @author Luther Root
 * 
 * 
 */
public class LevelCompleteScreen implements GestureListener, InputProcessor {

	Matrix4 screen;
	String text;
	String text2;
	DecimalFormat format = new DecimalFormat("0.00");
GameScreen game;
	public LevelCompleteScreen(GameScreen game)
	{
		this.game = game;
		screen = new Matrix4();
	screen.setToOrtho2D(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	text = "Level Completed!";
		
	}
	public void draw(SpriteBatch batch)
	{
		text2 = "Fastest Time: " + format.format(Root.prefs.getTime(new Vector2(Root.prefs.getWorld(),Root.prefs.getLevel()))) + " seconds";
		Gdx.input.setInputProcessor(this);
		batch.setProjectionMatrix(screen);
		batch.begin();
	
		
		Assets.font.draw(batch, text, Gdx.graphics.getWidth()/2 - Assets.font.getBounds(text).width/2, Gdx.graphics.getHeight()/2 + Assets.font.getBounds(text).height/2);
		Assets.font.draw(batch, text2, Gdx.graphics.getWidth()/2 - Assets.font.getBounds(text2).width/2, Gdx.graphics.getHeight()/3 + Assets.font.getBounds(text2).height/2);

		batch.end();
	}


	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean tap(float x, float y, int count, int button) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean longPress(float x, float y) {
		
		return false;
	}
	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		
		

		return false;
	}
	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		game.readyScreen.type = ReadyType.READY;
		game.fbo.dispose();
		
		game.loadNew(Root.prefs.nextLevel());
		System.out.println("Loading...");
		game.state = gameState.READY;	
		return false;
	}
	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
