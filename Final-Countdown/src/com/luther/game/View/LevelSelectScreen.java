package com.luther.game.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.luther.game.Assets;
import com.luther.game.Root;
import com.luther.game.ProjectSkin;
import com.luther.game.Model.Button;
import com.luther.game.Model.SelectableTextButton;
/**
 * <h1>Purpose: </h1> 
 *  	A screen to select levels, duh
 * 
 * @author Luther Root
 * 
 * 
 */
class Container {

	public Button button[] = new Button[5];
	public Rectangle bounds;

	public Container(float x, float y, float width, float height) {
		bounds = new Rectangle(x, y, width, height);
	}

}

public class LevelSelectScreen implements Screen, InputProcessor {

	Stage stage;
	Table table;
	List list;
	ScrollPane scrollPane;
	TextButton play, back;
	Window window;
	Window level;

	SpriteBatch batch;
	public Container container[] = new Container[5];
	int currentWorld = 0;
	ShapeRenderer shapes;
	Root game;
	float width = Root.gameWidth;
	float height = Root.gameHeight;
	SelectableTextButton[] worlds = new SelectableTextButton[5];
	

	public LevelSelectScreen(final Root game) {
		
		stage = new Stage(width, height, false);
		OrthographicCamera cam = new OrthographicCamera();
		cam.setToOrtho(false,width, height);
		cam.position.set(width/ 2, height  / 2, 0);
		stage.setCamera(cam);
		stage.getSpriteBatch().setProjectionMatrix(cam.projection);
		batch = stage.getSpriteBatch();
		batch.setProjectionMatrix(cam.combined);
		shapes = new ShapeRenderer();
		
		this.game = game;

	}

	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		this.drawButtons();
		for(int i = 0; i<5; i++)
			worlds[i].draw(batch);
		batch.end();
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
		
	//	Table.drawDebug(stage);

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(this);

		Gdx.input.setInputProcessor(this);

		table = new Table(Root.skin.skin);
		table.setBounds(0, 0, width, height);
		
		window = new Window("Worlds", Root.skin.skin);
		level = new Window("Levels", Root.skin.skin);

		table.debug();


		
		

		// play = new TextButton("Play", MyGdxGame.skin.skin);
		// play.pad(15);

		back = new TextButton("Back",  Root.skin.skin.get("menu", TextButtonStyle.class));
		back.pad(10);

		back.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {

				Gdx.input.setInputProcessor(Root.menuScreen.stage);
				game.setScreen(Root.menuScreen);
			}
		});

		
		window.pad(50);
		window.setMovable(false);
		level.setMovable(false);
		// table.add(scrollPane).width(table.getWidth()/4).pad(50);
		table.add(window).top();

		table.add(level).width(table.getWidth() / 2)
				.height(table.getHeight() / 1.5f);
		table.add(back).bottom().right();
		
		table.setHeight(height);
		table.setWidth(width);
		stage.addActor(table);
		fillCells();

	}



	@Override
	public void hide() {
		stage.clear();
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		stage.dispose();

	}

	private void fillCells() {
		float x = 0;
		float y = 0;
		int g = 0;
		for (int i = 0; i < 5; i++) {
			
			worlds[i] = new SelectableTextButton();
			worlds[i].setText("World " +(i+1), Assets.font);
			worlds[i].setPosition(new Vector2(100,height/1.7f -(i*height/10)));
			worlds[0].isSelected = true;

			// stage.stageToScreenCoordinates(size);
			Vector2 size = new Vector2(level.getX(), level.getY());
			level.localToStageCoordinates(size);
			table.localToStageCoordinates(size);
			stage.stageToScreenCoordinates(size);

			container[i] = new Container(width/ 2.63f, height/8,
					table.getCell(level).getMaxWidth(), table.getCell(level)
							.getMaxHeight());
			x = container[i].bounds.x;
			y = container[i].bounds.y + container[i].bounds.height / 2;
			g = 0;

			for (int j = 0; j < 5; j++) {
				if (g > 4) {
					y = y - 128;
					g = 0;
				}

				container[i].button[j] = new Button(
						(x + container[i].bounds.width / 4 * g) - (16 * g),
						y + 32, 64, 64); // divide into 4 places and move them
											// over to account for width
				g++;

			}

		}

	}

	private void drawButtons() {

		batch.end();
	/*	shapes.setProjectionMatrix(stage.getCamera().combined);
		shapes.begin(ShapeType.Line);
		for (int i = 0; i < 5; i++) {
			
			shapes.setColor(Color.WHITE);

			shapes.setColor(Color.BLUE);
			shapes.line(container[i].bounds.x, container[i].bounds.y,
					container[i].bounds.x + container[i].bounds.width,
					container[i].bounds.y); // bottom
			shapes.line(container[i].bounds.x, container[i].bounds.y,
					container[i].bounds.x, container[i].bounds.height); // first
																		// vertical
			shapes.line(container[i].bounds.x + container[i].bounds.width,
					container[i].bounds.y, container[i].bounds.x
							+ container[i].bounds.width,
					container[i].bounds.height);// Right
			shapes.line(container[i].bounds.x, container[i].bounds.height,
					container[i].bounds.x + container[i].bounds.width,
					container[i].bounds.height);// Top

		}
		shapes.end();
		// shapes.begin(ShapeType.Filled);
		// shapes.rect(level.getX(), level.getY(), level.getWidth(),
		// level.getHeight());
		// shapes.end();*/

		batch.begin();

		int l = Root.prefs.getFarthestWorld();
		int k = Root.prefs.getFarthestLevel();
		

		for (int j = 0; j < 5; j++) {
			 if(j>l)
				 worlds[j].disabled = true;
			 else worlds[j].disabled = false;
			 
			if (SelectableTextButton.getSelectedIndex(worlds) < l) {

				batch.draw(
						Assets.button,
						container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().x,
						container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().y,
						container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().width,
						container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().height);
			} else if (SelectableTextButton.getSelectedIndex(worlds) == l && j <= k) {
				batch.draw(
						Assets.button,
						container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().x,
						container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().y,
						container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().width,
						container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().height);
			} else {

				container[SelectableTextButton.getSelectedIndex(worlds)].button[j].disabled = true;
				batch.setColor(Color.GRAY);
				batch.draw(
						Assets.button,
						container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().x,
						container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().y,
						container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().width,
						container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().height);
				batch.setColor(Color.WHITE);

			}
			Assets.font.draw(batch, (SelectableTextButton.getSelectedIndex(worlds) + 1) + "-"
					+ (j + 1),
					container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().x + 5,
					container[SelectableTextButton.getSelectedIndex(worlds)].button[j].getBounds().y + 32);

			// container[i].button[j] = new Button(container[i].bounds.x + 64*i,
			// container[i].bounds.y, 32,32);
		}

	}

	@Override
	public boolean keyDown(int keycode) {
		stage.keyDown(keycode);
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		stage.keyUp(keycode);
		if (keycode == Keys.BACK) { // MyGdxGame.manager.clear();
			game.setScreen(new MenuScreen(game, false));
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		stage.keyTyped(character);
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		stage.touchDown(screenX, screenY, pointer, button);
		for (int i = 0; i < 5; i++) {
			if (container[SelectableTextButton.getSelectedIndex(worlds)].button[i]
					.pointerInside(new Vector2(screenX, -screenY
							+ Gdx.graphics.getHeight()))
					&& !container[SelectableTextButton.getSelectedIndex(worlds)].button[i].disabled) {
				// container[TButton.getSelectedIndex(worlds)].button[i]
				// use for future button down color change
			}

		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		stage.touchUp(screenX, screenY, pointer, button);
		
		Vector3 victor =new Vector3(screenX,screenY,0);
		stage.getCamera().unproject(victor);
	
		for(int j = 0; j<5; j++)
		{
			if(worlds[j].pointerInside(new Vector2(victor.x,victor.y))&& worlds[j].disabled == false)
			{
				worlds[j].isSelected = true;
				SelectableTextButton.selectChanged(j, worlds);
			}
		//j = 5;
		}
	
		for (int i = 0; i < 5; i++) {
			
			if (container[SelectableTextButton.getSelectedIndex(worlds)].button[i].pointerInside(new Vector2(victor.x,victor.y)) && !container[SelectableTextButton.getSelectedIndex(worlds)].button[i].disabled) {
				Gdx.input.setInputProcessor(Root.gameScreen.input);
				Root.gameScreen.loadNew(new Vector2(SelectableTextButton.getSelectedIndex(worlds), i));
				game.setScreen(Root.gameScreen);
				
				
			}

		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		stage.touchDragged(screenX, screenY, pointer);
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		stage.mouseMoved(screenX, screenY);
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		stage.scrolled(amount);
		return false;
	}

}