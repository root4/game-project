package com.luther.game.View;

import java.text.DecimalFormat;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.luther.game.Assets;
import com.luther.game.Root;
import com.luther.game.View.GameScreen.gameState;
/**
 * <h1>Purpose: </h1> 
 *  	Displays after a death, or when users start a new level
 * 
 * @author Luther Root
 * 
 * 
 */
public class ReadyScreen implements InputProcessor {
	
	public enum ReadyType{READY, DEADFALLING, DEADSPIKES, DEADBLADE };
	
	public ReadyType type;
	GameScreen game;
	Matrix4 screen;
	String text;
	String f;
	String s;
	String b;
	String[] falling;
	String[] spike;
	String[] blade;
	DecimalFormat format = new DecimalFormat("0.00");

	public ReadyScreen(GameScreen g1)
	{
		game = g1;
		type = ReadyType.READY;
		screen = new Matrix4();
		screen.setToOrtho2D(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		text= new String();
		FileHandle file = Gdx.files.internal("data/Words/falling.TXT");
		f= new String(file.readString());
		file = Gdx.files.internal("data/Words/blade.TXT");
		b = new String(file.readString());
		file = Gdx.files.internal("data/Words/spike.TXT");
		s = new String(file.readString());
		
		falling = f.split("\r\n");
		spike = s.split("\r\n");
		blade = b.split("\r\n");
		
		text = "";
	}
	
	
	public void draw(SpriteBatch batch)
	{
		this.update(); // for some reason it doesnt update before it draws on a new level
		Gdx.input.setInputProcessor(this);
		
		batch.setProjectionMatrix(screen);
		batch.begin();
		String text2 = "Time to beat: " +format.format( Root.prefs.getTime(new Vector2(Root.prefs.getWorld(),Root.prefs.getLevel()))) +" seconds";
		
		Assets.font.draw(batch, text, Gdx.graphics.getWidth()/2 - Assets.font.getBounds(text).width/2, Gdx.graphics.getHeight()/2 + Assets.font.getBounds(text).height/2);
		if(!(Root.prefs.getTime(new Vector2(Root.prefs.getWorld(),Root.prefs.getLevel())) == 1000.0f))
			Assets.font.draw(batch, text2, Gdx.graphics.getWidth()/2 - Assets.font.getBounds(text2).width/2, Gdx.graphics.getHeight()/3 + Assets.font.getBounds(text2).height/2);

		batch.end();
	}
	
	public void update()
	{
		if(text == "")
			{Random rn = new Random();
			switch(type)
			{
			
			
			case READY:
				text = "Ready?";
				break;
			case DEADFALLING:
				text = falling[rn.nextInt(5)];
				break;
			case DEADSPIKES:
				text = spike[rn.nextInt(5)];
				break;
			case DEADBLADE :
			text =  blade[rn.nextInt(5)];
			break;
			 default:
				text = "Ready?";
				break;
				
			}
			}
	}

	@Override
	public boolean keyDown(int keycode) {
Gdx.input.setInputProcessor(game.input);
		text = "";
		game.state = gameState.RUNNING;
		return false;
	}


	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		Gdx.input.setInputProcessor(game.input);
		text = "";
		game.state = gameState.RUNNING;
		return false;
	}


	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
