package com.luther.game.Controller;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.luther.game.Assets;
import com.luther.game.Model.Player;
import com.luther.game.Model.Player.restrictions;

/**
 * <h1>Purpose: </h1> 
 *  	Handle all input for GameScreen
 *  
 * 
 * @author Luther Root
 * 
 * 
 */
public class InputHandler implements InputProcessor{

	Player p;
	public InputHandler(Player p1)
	{
		p = p1;
	}

	public void getInput()
	{
		
		if (Gdx.input.isKeyPressed(Input.Keys.SPACE) && p.getRestrictions() != restrictions.nUP && p.canJump ==true) 
		{	
			Player.JUMP+=10;
			if(Player.JUMP > Player.MAXJUMP)
				Player.JUMP = Player.MAXJUMP;
		
		}
	
		if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) && p.getRestrictions() != restrictions.nRIGHT) {
			
			p.getVelocity().x +=30;
			
			Player.FRICTION = .9f;

			if (p.isFacingRight() == false) {
				p.setFacingRight(true);
				
			}
			
		} else if ((Gdx.input.isKeyPressed(Input.Keys.LEFT)) && p.getRestrictions() != restrictions.nLEFT) {
			
			p.getVelocity().x -=30;
			

			Player.FRICTION = .9f;
			if (p.isFacingRight() == true) {
				p.setFacingRight(false);
				
			}
		}
		else
			Player.FRICTION = .8f;

		if (p.getRestrictions() != restrictions.nDOWN & p.getVelocity().y > -650f) {
			p.getVelocity().y -= Player.GRAVITY ;
			
		}
		if(p.getRestrictions() != restrictions.nDOWN && p.getVelocity().y == 0f)
			Assets.fall.play();

			
		

		p.setRestrictions(restrictions.NONE);
	}
	
	public void getAndroidInput()
	{
		for (int i = 0; i < 2; i++) {

			if (Gdx.input.getX(i) > Gdx.graphics.getWidth() * .28f&& Gdx.input.getX(i) < Gdx.graphics.getWidth() / 1.9f&&
					-Gdx.input.getY(i) + Gdx.graphics.getHeight() < Gdx.graphics.getHeight() * .3f && p.getRestrictions() != restrictions.nRIGHT
					&& Gdx.input.isTouched(i)) 
			{
				
				p.getVelocity().x = Player.VELOCITY;
				Player.FRICTION = .9f;
				if (p.isFacingRight() == false) {
					p.setFacingRight(true);
					
				}
			}

			else if (Gdx.input.getX(i) > 0
					&& Gdx.input.getX(i) < Gdx.graphics.getWidth() * .28f
					&& -Gdx.input.getY(i) + Gdx.graphics.getHeight() < Gdx.graphics .getHeight() * .3f && p.getRestrictions() != restrictions.nLEFT
					&& Gdx.input.isTouched(i)) 
			{
				
				p.getVelocity().x = -Player.VELOCITY;
				Player.FRICTION = .9f;
				if (p.isFacingRight() == true) {
					p.setFacingRight(false);
				
				}
			}
			else 
				Player.FRICTION = .8f;
		}

		for (int k = 0; k < 2; k++) {

			if (-Gdx.input.getY(k) + Gdx.graphics.getHeight() < Gdx.graphics.getHeight() * .3f && Gdx.input.getX(k) > Gdx.graphics.getWidth() / 1.8f
					&& p.getRestrictions() != restrictions.nUP && Gdx.input.isTouched(k))
			{
				Player.JUMP +=10;
				if(Player.JUMP >Player.MAXJUMP)
					Player.JUMP = Player.MAXJUMP;
			
				
			
					

			}
			
		}

		if (p.getRestrictions() != restrictions.nDOWN & p.getVelocity().y > -650f) { // apply gravity until terminal velocity is reached
			p.getVelocity().y -= Player.GRAVITY;
			
		}

	
	
		p.setRestrictions(restrictions.NONE);
	}
	@Override
	public boolean keyDown(int keycode) {
		
	
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(keycode == Input.Keys.SPACE)
			p.jump();
		
		
		
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		
	
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
	
		if (-screenY + Gdx.graphics.getHeight() < Gdx.graphics.getHeight() * .3f && screenX > Gdx.graphics.getWidth() / 1.8f && Gdx.app.getType() == ApplicationType.Android )
		{ 
			p.jump();
		}
		
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
