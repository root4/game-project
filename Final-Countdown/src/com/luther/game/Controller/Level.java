package com.luther.game.Controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.luther.game.Assets;
import com.luther.game.Root;
import com.luther.game.Model.CollisionTile;
import com.luther.game.Model.Enemy;
import com.luther.game.Model.Player;
import com.luther.game.Model.CollisionTile.Dead;
import com.luther.game.Model.Player.Status;
import com.luther.game.Model.Player.restrictions;
import com.luther.game.View.ReadyScreen.ReadyType;

/**
 * <h1>Purpose: </h1> 
 *  	Load levels from file and initializes enemy & tile lists,
 *  	<br>also handles collisions 
 * 
 * @author Luther Root
 * 
 * 
 */

public class Level {

	private int BLOCKSIZE = 32;
	
	int index = 0;
	int writeIndex = 0;
	int rawNumber = 0;
	int finalNumber = 0;
	String st;
	int tempIndex = 0;
	int loadCounterX = 0, loadCounterY = 0;

	char MapFile[][] = new char[200][200];

	int placeHolder;
	public int mapSizeX, mapSizeY, R, G, B;
	public Rectangle oh = new Rectangle(-1, -1, -1, -1);

	private List<CollisionTile> colTiles;
	private List<Enemy> enemies;
	Rectangle futureGuy;


	public Level() {
		this.colTiles = new ArrayList<CollisionTile>();
		this.enemies = new ArrayList<Enemy>();
		
	}

	public void loadMap(String filename) {
		
		loadCounterX = 0; 
		loadCounterY = 0;
		index = 0;
		writeIndex = 0;
		tempIndex = 0;
		rawNumber = 0;
		finalNumber = 0;
		 mapSizeX = 0;
		 mapSizeY = 0;
		 
		 colTiles.clear();
		 enemies.clear();
		 
		 // Open the file.
		FileHandle file = Gdx.files.internal(filename);
		st = new String(file.readString());

		
		this.GetSizeRGB();
		index = tempIndex + 1;

		for (int i = index; i < st.length(); i++) {
			// if (Character.digit(st.charAt(i), 10) != -1) {
			if (st.charAt(i) != '\r' && st.charAt(i) != '\n') {

				if (Character.isLetter(st.charAt(i))) { // handles characters

					MapFile[loadCounterX][loadCounterY] = st.charAt(i);
				} else {								// handles numbers
					char[] car = new char[1];
					car = Character.toChars(st.charAt(i));
					MapFile[loadCounterX][loadCounterY] = car[0];
				}
				loadCounterX++;
				if (loadCounterX >= mapSizeX) {
					loadCounterX = 0;
					loadCounterY++;
					
				}

			}
		}
		this.newMap();

	}

	private void findFlags(CollisionTile t, char s) {

		switch (s) {
		case '1':
			
			t.dFlag = true;
			break;
		case '2':
			t.uFlag = true;
			break;
		case '3':
			t.lFlag = true;
			break;
		case '4':
			t.rFlag = true;
			break;
		case '5':
			t.lFlag = true;
			t.dFlag = true;
			break;
		case '6':
			t.rFlag = true;
			t.dFlag = true;
			break;
		case '7':
			t.lFlag = true;
			t.uFlag = true;
			break;
		case '8':
			t.rFlag = true;
			t.uFlag = true;
			break;
		case '9':
			break;
		case 'A':
			break;
		case 'B':
			break;
		case 'C':
			break;
		case 'D':
			t.lFlag = true;
			t.dFlag = true;
			t.uFlag = true;
			break;
		case 'E':
			t.uFlag = true;
			t.dFlag = true;
			break;
		case 'F':
			t.rFlag = true;
			t.dFlag = true;
			t.uFlag = true;
			break;
		case 'U':
			t.lFlag = true;
			t.rFlag = true;
			t.setDeadType(Dead.DOWN);
			break;
		case 'S':
			t.lFlag = true; 
			t.rFlag = true;
			t.setDeadType(Dead.UP);
			break;
		case 'W':
			t.dFlag = true;
			t.uFlag = true;
			t.rFlag = true;
			t.lFlag = true;
			t.setDeadType(Dead.WIN);

		}

	}

	public void GetSizeRGB() {

		while (index < 20) {
			if (Character.digit(st.charAt(index), 10) != -1) // if Character
																// does not
																// equal space
			{
				rawNumber = Character.digit(st.charAt(index), 10); // number is
																// equal to the
																// character
																// read in, at
																// base 10

				if (index != 0) {// if this is not the first character in the
									// file // hacks
					finalNumber = finalNumber * 10; // if first number in
													// sequence 1*10 = 10;

				}
				index++;// next letter
				finalNumber += rawNumber; // add number
				///System.out.println(Character.digit(st.charAt(index), 10));
			} else { // if there is a space, end number and put into holder

				switch (writeIndex) {
				case 0:
					mapSizeX = finalNumber;
					break;
				case 1:
					mapSizeY = finalNumber;
					break;
				case 2:
					R = finalNumber;
					break;
				case 3:
					G = finalNumber;
					break;
				case 4:
					B = finalNumber;
					break;
				}
				tempIndex = index + 1;
				if (writeIndex == 4) // if two -1 -1
					break;
				finalNumber = 0;
				writeIndex++;
				index++;

			}
		}


	}

	public void newMap() {

		Vector2 pos;
		int top = mapSizeY * BLOCKSIZE;

		for (int i = 0; i < mapSizeX; i++) {
			for (int j = 0; j < mapSizeY; j++) {

				CollisionTile tiles;

				char col = MapFile[i][j];
				if(MapFile[i][j]=='N')
				{
					Enemy e;
					e = new Enemy(i*BLOCKSIZE, top - (j* BLOCKSIZE),BLOCKSIZE,BLOCKSIZE);
					enemies.add(e);
				}
				
				if (MapFile[i][j] != '0') {
					pos = new Vector2(i * BLOCKSIZE, top - (j * BLOCKSIZE));

					tiles = new CollisionTile(pos, BLOCKSIZE, BLOCKSIZE,
							MapFile[i][j]);

					findFlags(tiles, col);
					colTiles.add(tiles);

				}

			}
		}
		 this.Sort();
	}

	

	public void CD(Player p1, float dt) {
		futureGuy = p1.getBounds();

		futureGuy.x += (p1.getVelocity().x * dt);
		futureGuy.y += p1.getVelocity().y * dt + 1;

		if(futureGuy.y < -800)
		{
			Root.gameScreen.readyScreen.type = ReadyType.DEADFALLING;
			p1.setState(Status.DEAD);
		}
		
		
	coll:	for (CollisionTile tile: colTiles) {
			
			
			if (this.intersects(tile.getBounds(), futureGuy)) {
				
				if (tile.getDeadType() == Dead.WIN) {
					p1.setState(Status.WIN);
					
				}
				if (tile.getDeadType() == Dead.DOWN || tile.getDeadType()==Dead.UP)
				{Root.gameScreen.readyScreen.type = ReadyType.DEADSPIKES;
					p1.setState(Status.DEAD);
				}

				if (oh.height > oh.width) {
					if (tile.lFlag == true && (oh.getX() + oh.getWidth()> futureGuy.getX() + (p1.getWidth()/4)) && p1.getVelocity().x >0) {
					
						
							p1.setRestrictions(restrictions.nRIGHT);
							p1.setVelocity(0, p1.getVelocity().y);
							System.out.println("RightCol");
							// p1.move(-oh.width, 0);
							break coll;
						
					}
					if (tile.rFlag == true && (oh.getX()<futureGuy.getX() +(p1.getWidth()/4))&&p1.getVelocity().x<0) {
						
							p1.setRestrictions(restrictions.nLEFT);
							p1.setVelocity(0, p1.getVelocity().y);
							// p1.move(oh.width, 0);
							//System.out.println("LeftCol");
							break coll;
						
					}
				

				}
				if (oh.width > oh.height) {

					if ((p1.getVelocity().y<0)) {

						if (oh.getY() < futureGuy.getY()
								+ (futureGuy.getHeight() / 4)) {
							/*if (tile.getDeadType() == Dead.DOWN)
							{Root.gameScreen.readyScreen.type = ReadyType.DEADSPIKES;
								p1.setState(Status.DEAD);
							}*/
							if (tile.dFlag == true) {
								// System.out.println("DownCol");
								p1.setRestrictions(restrictions.nDOWN);
								p1.setVelocity(p1.getVelocity().x, 0.000000f);
								if(p1.canJump == false)
									{ Assets.jump.stop();
											Assets.fall.play();
									p1.canJump = true;
									}
								break coll;
							}

							// p1.move(0, oh.height);
						}

					} else if ((p1.getVelocity().y>0)) {
						if (oh.getY() + oh.getHeight() > futureGuy.getY()
								+ (futureGuy.getHeight() / 8)) {
							if (tile.getDeadType() == Dead.UP)
							{
								Root.gameScreen.readyScreen.type = ReadyType.DEADSPIKES;
								p1.setState(Status.DEAD);
							}
							if (tile.uFlag == true) {
								
								System.out.println("UpCol");
								p1.setRestrictions(restrictions.nUP);
								p1.setVelocity(p1.getVelocity().x, 0.000f);
								// p1.move(0, -oh.height-1);
								break coll;
							}

						}

					}

				}
			
				
			
				

			}

		}

	}
	public void CDE(Player p1){
		float l = enemies.size();
		for(CollisionTile tile: colTiles)
			if(tile.getTileType() == 'L')
			{
				for (Enemy enemy: enemies)
				{
		
					enemy.checkReverse(tile);
			
					if(this.intersects(enemy.getBounds(),futureGuy))
						{
							p1.setState(Status.DEAD);
							Root.gameScreen.readyScreen.type = ReadyType.DEADBLADE;
						}
				}
			}
	}

	public boolean intersects(Rectangle player1, Rectangle world) {
		float interLeft = Math.max(world.x, player1.x);
		float interTop = Math.max(world.y, player1.y);
		float interRight = Math.min(world.x + world.width, player1.x
				+ player1.width);
		float interBottom = Math.min(world.y + world.height, player1.y
				+ player1.height);

		if ((interLeft < interRight) && (interTop < interBottom)) {
			oh = new Rectangle(interLeft, interTop, interRight - interLeft,
					interBottom - interTop);
			return true;
		} else {
			oh = new Rectangle(0, 0, 0, 0);
			return false;
		}
	}

	public void Sort() {
		
	Collections.sort(colTiles);
	}

	
	public List<CollisionTile> getColTiles()
	{
		return colTiles;
	}
	public List<Enemy> getEnemies()
	{
		return enemies;
	}
}
