

package com.luther.game;

import java.io.File;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
/**
 * Purpose - Manages all assets used
 * 
 * @author Luther Root
 * 
 * 
 */
public class Assets {

	public static Animation world;
	public static Animation bird;
	public static Texture clouds;
	public static TextureRegion wcloud, swcloud, dcloud, sdcloud, sun;

	public static TextureRegion up;
	public static TextureRegion down;

	public static Animation walk;
	public static Animation walkLeft;
	private static Texture heroSheet;
	public static TextureRegion idleRight;
	public static TextureRegion idleLeft;
	public static TextureRegion frame;
	public static TextureRegion falling;
	public static TextureRegion jumping;
	public static TextureRegion fallingL;
	public static TextureRegion jumpingL;

	public static Texture mapTexture;
	public static Texture spike;
	public static TextureRegion one;
	public static TextureRegion two;
	public static TextureRegion three;
	public static TextureRegion four;
	public static TextureRegion five;
	public static TextureRegion six;
	public static TextureRegion seven;
	public static TextureRegion eight;
	public static TextureRegion nine;
	public static TextureRegion A;
	public static TextureRegion B;
	public static TextureRegion C;
	public static TextureRegion D;
	public static TextureRegion E;
	public static TextureRegion F;
	public static TextureRegion Z;
	public static TextureRegion S;
	public static TextureRegion U;
	public static Texture door;
	public static TextureRegion back;
	public static Texture blade;

	public static BitmapFont font;
	public static BitmapFont headerFont;
	public static Texture select;

	public static Texture pauseButton;

	public static Texture control;
	public static TextureRegion controlRight;
	public static TextureRegion controlLeft;
	public static TextureRegion controlUp;

	public static Texture button;
	public static Texture help;
	
	public static Sound jump;
	public static Sound fall;
	public static Sound step;
	public static Sound doorSound;

	public static void load() {
		
		jump = Gdx.audio.newSound(Gdx.files.internal("asd.wav"));
		fall = Gdx.audio.newSound(Gdx.files.internal("fall.wav"));
		step = Gdx.audio.newSound(Gdx.files.internal("step.wav"));
		doorSound = Gdx.audio.newSound(Gdx.files.internal("door.wav"));
		TextureRegion frames[] = new TextureRegion[5];
		frames[0] = new TextureRegion(new Texture(
				Gdx.files.internal("data/opening/one.png")), 0, 0, 121, 122);
		frames[1] = new TextureRegion(new Texture(
				Gdx.files.internal("data/opening/two.png")), 0, 0, 121, 121);
		frames[2] = new TextureRegion(new Texture(
				Gdx.files.internal("data/opening/three.png")), 0, 0, 121, 121);
		frames[3] = new TextureRegion(new Texture(
				Gdx.files.internal("data/opening/four.png")), 0, 0, 121, 121);
		frames[4] = new TextureRegion(new Texture(
				Gdx.files.internal("data/opening/five.png")), 0, 0, 121, 121);
		world = new Animation(.2f, frames);

		frames = new TextureRegion[4];
		frames[0] = new TextureRegion(new Texture(
				Gdx.files.internal("bird.png")), 0, 0, 16, 16);
		frames[1] = new TextureRegion(new Texture(
				Gdx.files.internal("bird.png")), 16, 0, 16, 16);
		frames[2] = new TextureRegion(new Texture(
				Gdx.files.internal("bird.png")), 32, 0, 16, 16);
		frames[3] = new TextureRegion(new Texture(
				Gdx.files.internal("bird.png")), 48, 0, 16, 16);
		bird = new Animation(.1f, frames);
		bird.setPlayMode(Animation.LOOP);

		clouds = new Texture(Gdx.files.internal("clouds.png"));
		sun = new TextureRegion(clouds, 0, 0, 31, 28);
		wcloud = new TextureRegion(clouds, 41, 0, 34, 17);
		swcloud = new TextureRegion(clouds, 37, 26, 38, 8);
		dcloud = new TextureRegion(clouds, 95, 0, 34, 17);
		sdcloud = new TextureRegion(clouds, 90, 25, 38, 9);

		up = new TextureRegion(new Texture(Gdx.files.internal("up.png")), 0, 0,
				64, 32);
		down = new TextureRegion(new Texture(Gdx.files.internal("down.png")),
				0, 0, 64, 32);

		button = new Texture(Gdx.files.internal("button.png"));

		control = new Texture(Gdx.files.internal("controlsA.png"));

		controlRight = new TextureRegion(control, 0, 0, 256, 86);
		controlLeft = new TextureRegion(control, 0, 82, 256, 86);
		controlUp = new TextureRegion(control, 0, 172, 256, 84);

		pauseButton = new Texture(Gdx.files.internal("pause.png"));

		font = new BitmapFont(Gdx.files.internal("data/Fonts/new.fnt"), false);
		headerFont = new BitmapFont(
				Gdx.files.internal("data/Fonts/header.fnt"), false);

		heroSheet = new Texture(Gdx.files.internal("data/guy15.png"));

		idleRight = new TextureRegion(heroSheet, 130, 0, 32, 32);
		idleLeft = new TextureRegion(idleRight);
		idleLeft.flip(true, false);
		falling = new TextureRegion(heroSheet, 160, 0, 32, 32);
		fallingL = new TextureRegion(falling);
		fallingL.flip(true, false);
		jumping = new TextureRegion(heroSheet, 192, 0, 32, 32);
		jumpingL = new TextureRegion(jumping);
		jumpingL.flip(true, false);

		help = new Texture(Gdx.files.internal("data/help.png"));
		loadAnimation();

	}

	public static void loadLevel(String Directory) {
		levelDispose();
		mapTexture = new Texture(Directory + "pickleTile.png");
		spike = new Texture(Directory + "spike.png");
		door = new Texture("data/door.png");
		back = new TextureRegion(new Texture(Directory + "background.png"));
		blade = new Texture("data/blade.png");

		one = new TextureRegion(mapTexture, 32, 0, 32, 32);
		two = new TextureRegion(mapTexture, 64, 0, 32, 32);
		three = new TextureRegion(mapTexture, 384, 0, 32, 32);
		four = new TextureRegion(mapTexture, 352, 0, 32, 32);
		five = new TextureRegion(mapTexture, 0, 0, 32, 32);
		six = new TextureRegion(mapTexture, 128, 0, 32, 32);
		seven = new TextureRegion(mapTexture, 192, 0, 32, 32);
		eight = new TextureRegion(mapTexture, 160, 0, 32, 32);
		nine = new TextureRegion(mapTexture, 256, 0, 32, 32);
		A = new TextureRegion(mapTexture, 224, 0, 32, 32);
		B = new TextureRegion(mapTexture, 320, 0, 32, 32);
		C = new TextureRegion(mapTexture, 288, 0, 32, 32);
		D = new TextureRegion(mapTexture, 480, 0, 32, 32);
		E = new TextureRegion(mapTexture, 416, 0, 32, 32);
		F = new TextureRegion(mapTexture, 448, 0, 32, 32);
		Z = new TextureRegion(mapTexture, 96, 0, 32, 32);
		U = new TextureRegion(spike, 0, 0, 32, 32);
		S = new TextureRegion(spike, 0, 0, 32, 32);
		S.flip(false, true);
	}

	public static void levelDispose() {
		if (mapTexture != null) {
			mapTexture.dispose();
			spike.dispose();

		}
	}

	public static void loadAnimation() {

		TextureRegion[] walkFrames = new TextureRegion[4];
		TextureRegion[] walkLeftFrames = new TextureRegion[4];
		for (int i = 0; i < 4; i++) {
			walkFrames[i] = new TextureRegion(heroSheet, i * 32, 0, 32, 32);
			walkLeftFrames[i] = new TextureRegion(heroSheet, i * 32, 0, 32, 32);
			walkLeftFrames[i].flip(true, false);
		}
		walk = new Animation(.1f, walkFrames);
		walkLeft = new Animation(.1f, walkLeftFrames);
	}

	public static void dispose() {

		clouds.dispose();

		heroSheet.dispose();
		try{
			mapTexture.dispose();
			spike.dispose();
			door.dispose();	
			blade.dispose();
		}
		catch(NullPointerException e)
		{
			//This happens when player exits before playing a level
			// Only way to handle it is to do nothing because they are null
		}

		font.dispose();
fall.dispose();
jump.dispose();
		pauseButton.dispose();

		control.dispose();

		button.dispose();

	}
}
