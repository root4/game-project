package com.luther.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.luther.game.View.GameScreen;
import com.luther.game.View.HelpScreen;
import com.luther.game.View.MenuScreen;
import com.luther.game.View.LevelSelectScreen;
import com.luther.game.View.SplashScreen;
/**
 * <h1>Purpose: </h1> 
 *  	Insertion point for application, creates all
 *  	<br>screens statically to be used throughout the lifecycle. 
 * 
 * @author Luther Root
 * 
 * 
 */
public class Root extends Game {

	public static MenuScreen menuScreen;
	public static SplashScreen splashScreen;
	public static GameScreen gameScreen;
	public static HelpScreen helpScreen;

	public static LevelSelectScreen lScreen;

	public static GamePreferences prefs;
	public static ProjectSkin skin;

	public static float gameWidth = 800;
	public static float gameHeight = 480;

	@Override
	public void create() {

		Gdx.input.setCatchBackKey(true);

		Assets.load();

		skin = new ProjectSkin();

		lScreen = new LevelSelectScreen(this);

		prefs = new GamePreferences();

		gameScreen = new GameScreen(this);
		gameScreen.init();

		menuScreen = new MenuScreen(this, true);

		splashScreen = new SplashScreen(this);
		helpScreen = new HelpScreen(this);

		super.setScreen(splashScreen);

	}

	@Override
	public void resize(int width, int height) {

		super.resize(width, height);
	}

	@Override
	public void render() {

		super.render();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();

	}

	@Override
	public void dispose() {
		super.dispose();
		Assets.dispose();

	}

}
	
	


