package com.luther.game.Model;

import com.badlogic.gdx.math.Vector2;
/**
 * <h1>Purpose: </h1> 
 *  	An object with velocity
 * 
 * @author Luther Root
 * 
 * 
 */
public class DynamicGameObject extends GameObject {
	
	protected Vector2 velocity;
	
	public DynamicGameObject(float x, float y, float width, float height)
	{
		super(x,y,width,height);
		velocity = new Vector2(0,0);
		
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

	public void setVelocity(float x, float y) {
		velocity.set(x, y);
	}


}
