package com.luther.game.Model;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.luther.game.Assets;

/**
 * <h1>Purpose: </h1> 
 *  	Used to create our main character,
 *  	<br>can draw itself, jump, and die
 * 
 * @author Luther Root
 * 
 * 
 */
public class Player extends DynamicGameObject {

	public enum Status {
		ALIVE, DEAD, WIN
	};

	public enum restrictions {
		NONE, nLEFT, nRIGHT, nDOWN, nUP
	};

	private Status state;
	restrictions canMove;

	public static final float GRAVITY = 35;
	public static final float VELOCITY = 300;
	public static float JUMP = 600;
	public static final float MAXJUMP = 1000;
	public static float FRICTION = .9f;

	private boolean facingRight = false;
	public boolean canJump = false;
	

	float statetime = 0;

	private TextureRegion frame;

	public Player(float x, float y, float width, float height) {
		super(x, y, width, height);
		setState(Status.ALIVE);
		bounds.setWidth(40);
		bounds.setHeight(40);

	}

	public void dispose() {
		// mSheet.dispose();
	}

	public void draw(SpriteBatch batch) {

		frame = isFacingRight() ? Assets.idleRight : Assets.idleLeft;
		if (velocity.x > 30 || velocity.x < -30)
		{
			frame = isFacingRight() ? Assets.walk.getKeyFrame(statetime, true)
					: Assets.walkLeft.getKeyFrame(statetime, true);
			
		}
		

		if (velocity.y > 60) {

			frame = isFacingRight() ? Assets.jumping : Assets.jumpingL;
			
		}
		if (velocity.y < -60) {

			frame = isFacingRight() ? Assets.falling : Assets.fallingL;
		}

		// if else to put playing image within bounds
		if (isFacingRight() == false)
			batch.draw(frame, position.x - bounds.getWidth() / 2, position.y,
					60, 60);
		else
			batch.draw(frame, position.x, position.y, 60, 60);

	}

	
	public void update(float dt) {

		statetime += dt;

		position.add(velocity.x * dt, velocity.y * dt);
		velocity.x = velocity.x * FRICTION; // friction

	}

	public void jump() {
		if (JUMP > MAXJUMP)
			JUMP = MAXJUMP;

		if (velocity.y < -15)
			canJump = false;

		if (canJump == true) {
			if (JUMP < 750)
				JUMP = 640;

			velocity.y = JUMP;
			Assets.jump.play();
		}

		JUMP = 600;
		canJump = false;
	}

	public restrictions getRestrictions() {
		return canMove;

	}

	public void setRestrictions(restrictions res) {
		canMove = res;
	}

	public Status getState() {
		return state;
	}

	public void setState(Status state) {
		this.state = state;
	}

	public void Reset() {
		position.set(90, 120);
		updateBounds();
		JUMP = 600;
		state = Status.ALIVE;
	}

	public void runDraw(SpriteBatch batch) {
		frame = Assets.walk.getKeyFrame(statetime, true);
		batch.draw(frame, position.x, position.y, 60, 60);
	}

	public boolean isFacingRight() {
		return facingRight;
	}

	public void setFacingRight(boolean facingRight) {
		this.facingRight = facingRight;
	}

}
