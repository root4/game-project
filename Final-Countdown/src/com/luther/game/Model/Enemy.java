package com.luther.game.Model;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.luther.game.Assets;
/**
 * <h1>Purpose: </h1> 
 *  	A Dynamic game object that can draw itself,
 *  	<br>used for saw blades ingame.
 * 
 * @author Luther Root
 * 
 * 
 */
public class Enemy extends DynamicGameObject {
	

	TextureRegion enemyRegion;
	float rotation = 0;

	public Enemy(float f, float g, float h, float i) {
		super(f, g, h, i);
		enemyRegion = new TextureRegion(Assets.blade,0,0,32,32);
		velocity.set(250, 0);
		bounds.width = bounds.width/2;
		bounds.height = bounds.height/2;
		
	}
	public void drawAndUpdateEnemy(SpriteBatch batch)
	{
	
		
		batch.draw(enemyRegion, position.x, position.y,bounds.width/2 ,bounds.height/2, bounds.width,bounds.height,1,1,rotation);
		
		
	}
	public void update()
	{	rotate();
		position.x += velocity.x * 1/60f;
		bounds.setPosition(position);
		
	}
	public void reverse()
	{
		velocity.x = -velocity.x;
		
		
		
	}
private void rotate()
{
	if(velocity.x >0)
		rotation-=10f;
	else
		rotation+=10f;
}
	public void checkReverse(CollisionTile tile)
	{
		float interLeft = Math.max(tile.getPosition().x, position.x);
		float interRight = Math.min(tile.getPosition().x +32, position.x
				+ bounds.width);
		float interTop = Math.max(tile.getPosition().y, position.y);
		float interBottom = Math.min(tile.getPosition().y + 32, position.y
				+ bounds.height);
		if((interLeft < interRight) && (interTop<interBottom))
			reverse();
	}

}
