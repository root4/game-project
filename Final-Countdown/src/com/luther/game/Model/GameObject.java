package com.luther.game.Model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
/**
 * <h1>Purpose: </h1> 
 *  	Used to create objects with position,
 *  	<br>and a bounding rectangle. 
 * 
 * @author Luther Root
 * 
 * 
 */
public abstract class GameObject {

	protected Vector2 position;
	
	protected Rectangle bounds = new Rectangle();
	

	public GameObject(float f, float g, float h, float i) {
		position = new Vector2(f,g);

		bounds.set(f, g, h, i);
	}


	public Vector2 getPosition() {
		return position;
	}


	public void setPosition(Vector2 position) {
		this.position = position;
	}


	public Rectangle getBounds() {
		updateBounds();
		return bounds;
	}


	public void setBounds(Rectangle bounds) {
		this.bounds = bounds;
	}
	public void updateBounds() {
		bounds.set(position.x, position.y, bounds.width, bounds.height);
	}

	public float getHeight() {
		return bounds.height;
	}

	public float getWidth() {
		return bounds.width;
	}
	
}