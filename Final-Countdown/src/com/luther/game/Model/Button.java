package com.luther.game.Model;

import com.badlogic.gdx.math.Vector2;
/**
 * <h1>Purpose: </h1> 
 *  	A Button that can be disabled and has position
 * 
 * @author Luther Root
 * 
 * 
 */

public class Button extends GameObject{

	public boolean disabled;
	public Button(float f, float g, float h, float i) {
		super(f, g, h, i);
		disabled= false;
		
	}
	
public boolean pointerInside(Vector2 point){
		
		if(point.x >this.position.x && point.x < (this.position.x + this.bounds.getWidth()) && point.y > this.position.y && point.y < (this.position.y+this.bounds.getHeight()))
			return true;
		else
			return false;
		
		
		
	}


}


