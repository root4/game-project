package com.luther.game.Model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
/**
 * <h1>Purpose: </h1> 
 *  	A selectable text button 
 * 
 * @author Luther Root
 * 
 * 
 */
public class SelectableTextButton extends Button {
	String text;
	BitmapFont font;
	public boolean isSelected = false;
	Pixmap pixmap;
	int pad = 10;
	
	public SelectableTextButton()
	{
		super(0,0,0,0);
	}
	public SelectableTextButton(float f, float g, float h, float i) {
		super(f, g, h, i);
		disabled = true;
	
	}
	
	public void setText(String text,BitmapFont font)
	{
		this.text = text;
		this.font = font;
		this.bounds.width = font.getBounds(text).width;
		this.bounds.height = font.getBounds(text).height;
		pixmap = new Pixmap((int) this.bounds.width,((int) this.bounds.height + pad),Format.RGBA8888);
		pixmap.setColor(Color.BLUE);
		pixmap.fillRectangle(0, 0, (int)this.bounds.width, (int) this.bounds.height +pad );
	}

	public void draw(SpriteBatch batch)
	{
		if(isSelected)
			batch.draw(new Texture(pixmap),position.x ,position.y);
		
		if(disabled)
			font.setColor(Color.GRAY);
		
		
		font.draw(batch, text, position.x, position.y+ bounds.height +pad/2);
		font.setColor(Color.WHITE);
	}
	public static void selectChanged(int i, SelectableTextButton[] list) {

		for(int j = 0; j < list.length; j++)
		{
			if(j!=i)
				list[j].isSelected = false;
		}
		
	}
	public static int getSelectedIndex(SelectableTextButton[] list)
	{
		for(int i = 0; i  <list.length; i++)
			if(list[i].isSelected)
				return i;
		
		
		return -1;
	}
	
}
