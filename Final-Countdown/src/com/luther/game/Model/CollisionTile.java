package com.luther.game.Model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
/**
 * <h1>Purpose: </h1> 
 *  	Used to hold collision and drawing
 *  	<br>information for a single tile
 * 
 * @author Luther Root
 * 
 * 
 */
public class CollisionTile extends GameObject implements Comparable<CollisionTile>{

	public CollisionTile(float f, float g, float h, float i) {
		super(f, g, h, i);
		// TODO Auto-generated constructor stub
	}

	public enum Dead {
		NONE,LEFT, RIGHT, UP, DOWN,  WIN
	};

	
	private char tileType;
	private Dead deadType;
	public boolean lFlag, rFlag, uFlag, dFlag;
	boolean isItem;
	boolean isItemValid;
	
	public CollisionTile(Vector2 position, float width, float height, char id) {
		super(position.x,position.y,width,height);
		
		lFlag = false;
		rFlag = false;
		uFlag = false;
		dFlag = false;
		setDeadType(Dead.NONE);
		setTileType(id);

	}

	public CollisionTile(CollisionTile a) {
		super(a.position.x,a.position.y,a.getWidth(),a.getHeight());
		
		lFlag = a.lFlag;
		rFlag = a.rFlag;
		uFlag = a.uFlag;
		dFlag = a.dFlag;
		setDeadType(a.getDeadType());
		setTileType(a.getTileType());
	}

	

	@Override
	public int compareTo(CollisionTile o) {
	return this.getDeadType().compareTo(o.getDeadType());
	}

	public char getTileType() {
		return tileType;
	}

	public void setTileType(char tileType) {
		this.tileType = tileType;
	}

	public Dead getDeadType() {
		return deadType;
	}

	public void setDeadType(Dead deadType) {
		this.deadType = deadType;
	}
}
