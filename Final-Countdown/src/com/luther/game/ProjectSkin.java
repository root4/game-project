package com.luther.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
/**
 * Purpose - A hideous monster that creates a universal skin to be used on all scene2d elements
 * 
 * @author Luther Root
 * 
 * 
 */
public class ProjectSkin {

	public  Skin skin;
	Pixmap pixmap;
	Pixmap pausePix;
	TextButtonStyle textButtonStyle;
	ImageButtonStyle imgbtnStyle;
	LabelStyle labelStyle;
	ListStyle listStyle;
	ScrollPaneStyle spStyle;
	WindowStyle winStyle;
	public ProjectSkin()
	{
		skin = new Skin();
		
		
		 pixmap = new Pixmap(1, 1, Format.RGBA8888);
		 pausePix = new Pixmap(1,1,Format.RGBA8888);
		 pausePix.setColor(Color.GRAY);
		 pausePix.fill();
	
		pixmap.setColor(Color.BLUE);
		pixmap.fill();
		skin.add("white", new Texture(pixmap));
		skin.add("pause", new Texture(pausePix));
		
		
		skin.add("default", Assets.font);
		
		spStyle = new ScrollPaneStyle();
		skin.add("default", spStyle);
		TextureRegion back = new TextureRegion(new Texture(Gdx.files.internal("up.png")));

		
		 textButtonStyle = new TextButtonStyle();
	 labelStyle = new LabelStyle();

		textButtonStyle.up = new TextureRegionDrawable(back);
		//textButtonStyle.down = skin.newDrawable("white", Color.WHITE);
		//.checked = skin.newDrawable("white", Color.WHITE);
		//textButtonStyle.over =skin.newDrawable("white", Color.WHITE);
		textButtonStyle.font = skin.getFont("default");
		listStyle = new ListStyle();
		
		listStyle.font = Assets.font;
	
		listStyle.selection = skin.newDrawable("white", 0,1, 1, 1);
		skin.add("default", listStyle);

		imgbtnStyle = new ImageButtonStyle();
		//imgbtnStyle.
		skin.add("default", imgbtnStyle);
		
	//	font.scale(1.3f);
		winStyle = new WindowStyle();
		winStyle.titleFont = Assets.headerFont;
		skin.add("default", winStyle);
	
	
	
		

		skin.add("default", textButtonStyle);	
		labelStyle.font = skin.getFont("default");
		skin.add("default", labelStyle);
		
		//******MENUSCREEN*****************
		

		 TextButtonStyle textBStyle = new TextButtonStyle();
		 labelStyle = new LabelStyle();

		textBStyle.up = new TextureRegionDrawable(Assets.up);
		textBStyle.down = new TextureRegionDrawable(Assets.down);
		//textButtonStyle.checked = skin.newDrawable("white", Color.WHITE);
		//textBStyle.over =new TextureRegionDrawable(Assets.down);
		textBStyle.font = skin.getFont("default");
	
	
		

		skin.add("menu", textBStyle);	
		labelStyle.font = skin.getFont("default");
		skin.add("menu", labelStyle);
		
		//**********Pause
		
		TextButtonStyle pstyle = new TextButtonStyle();
		pstyle.up = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("data/pbutton.png"))));
		pstyle.down = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("data/pdbutton.png"))));
		pstyle.font = skin.getFont("default");
		skin.add("pause", pstyle);
		
		WindowStyle wStyle = new WindowStyle();
		wStyle.titleFont = Assets.headerFont;
		wStyle.background  = skin.newDrawable("pause",new Color(1,1,1,0.5f));
		skin.add("pause", wStyle);
		
		
	}
}
