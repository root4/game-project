package com.luther.game;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
/**
 * <h1>Purpose: </h1> 
 *  	Used to keep utility functions
 * 
 * @author Luther Root
 * 
 * 
 */
public class Utilities {

	
	public static boolean pointerInside(Vector2 point, Rectangle box){
		
		if(point.x >box.x && point.x < (box.x + box.height) && point.y > box.y && point.y < (box.y+box.height))
			return true;
		else
			return false;
		
		
		
	}
}
