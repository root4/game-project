package com.luther.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.Vector2;

/**
 * Purpose - used to create and manage user preferences including level
 * management
 * 
 * @author Luther Root
 * 
 * 
 */
public class GamePreferences {

	private static final String CURRENTWORLD = new String("CURRENTWORLD");
	private static final String FARTHESTWORLD = new String("FARTHESTWORLD");
	private static final String CURRENTLEVEL = new String("CURRENTLEVEL");
	private static final String FARTHESTLEVEL = new String("FARTHESTLEVEL");
	private static final String PREFS_NAME = "game";

	private final static int WORLDS = 5;
	public final static int LEVELS = 20;
	public static boolean BeatenLevels[][] = new boolean[WORLDS][LEVELS];

	public GamePreferences() {
		Preferences prefs = getPrefs();

		if (!prefs.getBoolean("Init")) {  // Sets default time of every level to 1000s on initial install
			for (int i = 0; i < 5; i++)
				for (int j = 0; j < 5; j++)
					this.setTime(new Vector2(i, j), 1000f);

			prefs.putBoolean("Init", true);
		}

	}

	protected Preferences getPrefs() {
		return Gdx.app.getPreferences(PREFS_NAME);
	}

	public void setLevel(int level) {

		Preferences prefs = getPrefs();

		prefs.putInteger(CURRENTLEVEL, level);
		prefs.flush();
	}

	public int getLevel() {
		return getPrefs().getInteger(CURRENTLEVEL);

	}

	public void setWorld(int world) {

		Preferences prefs = getPrefs();

		prefs.putInteger(CURRENTWORLD, world);
		prefs.flush();
	}

	public int getWorld() {
		return getPrefs().getInteger(CURRENTWORLD);
	}

	public int getFarthestLevel() {
		return getPrefs().getInteger(FARTHESTLEVEL);

	}

	public void setFarthestLevel(int level) {
		Preferences prefs = getPrefs();

		prefs.putInteger(FARTHESTLEVEL, level);
		prefs.flush();
	}

	public int getFarthestWorld() {
		return getPrefs().getInteger(FARTHESTWORLD);
	}

	public void setFarthestWorld(int world) {
		Preferences prefs = getPrefs();

		prefs.putInteger(FARTHESTWORLD, world);
		prefs.flush();
	}

	public Vector2 nextLevel() {
		int level = 0;
		int world = 0;
		world = this.getWorld();
		level = this.getLevel() + 1;
		if (world == this.getFarthestWorld()) {
			if (level > this.getFarthestLevel())
				this.setFarthestLevel(level);
		}
		if (level > 4) {

			level = 0;

			world++;
			if (world > this.getFarthestWorld()) {
				this.setFarthestLevel(0);
				this.setFarthestWorld(world);

			}

		}

		return new Vector2(world, level);
	}

	public void setTime(Vector2 level, float time) {
		Preferences prefs = getPrefs();

		if (getTime(level) >= time || getTime(level) == 0.0f)
			prefs.putFloat(level.x + ":" + level.y, time);

		prefs.flush();
	}

	public float getTime(Vector2 level) {
		float time = getPrefs().getFloat(level.x + ":" + level.y);

		return time;
	}

}
