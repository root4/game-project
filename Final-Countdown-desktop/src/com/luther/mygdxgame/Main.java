package com.luther.mygdxgame;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.luther.game.Root;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Final-Countdown";
		cfg.width = (int) Root.gameWidth;
		cfg.height = (int) Root.gameHeight;
		cfg.useGL20 =true;
		cfg.vSyncEnabled = false;
		cfg.foregroundFPS = 60;
		
		new LwjglApplication(new Root(), cfg);
	}
}
